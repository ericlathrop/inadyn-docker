# Inadyn Docker

An unofficial Docker setup for [Inadyn](https://github.com/troglobit/inadyn)
that tags images based on releases, so you don't have to use `latest`. The image
is pushed to
[`ericlathrop/inadyn`](https://hub.docker.com/r/ericlathrop/inadyn).
