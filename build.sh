#!/bin/bash
set -e

version=2.8.1
tag=ericlathrop/inadyn:${version}
docker build -t "$tag" --build-arg "version=$version" .
docker push "$tag"
