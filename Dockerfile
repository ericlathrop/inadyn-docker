FROM alpine:latest

ARG version

RUN apk --no-cache add \
  autoconf \
  automake \
  confuse-dev \
  gcc \
  git \
  gnutls-dev \
  libc-dev \
  libtool \
  make

WORKDIR /
RUN \
      wget -O inadyn.tar.gz https://github.com/troglobit/inadyn/releases/download/v${version}/inadyn-${version}.tar.gz && \
      tar xf inadyn.tar.gz && \
      cd inadyn-${version} && \
      ./configure --prefix=/usr --sysconfdir=/etc --localstatedir=/var && \
      make install

FROM alpine:latest

RUN apk --no-cache add \
  ca-certificates \
  confuse \
  gnutls

COPY --from=0 /usr/sbin/inadyn /usr/sbin/inadyn
COPY --from=0 /usr/share/doc/inadyn /usr/share/doc/inadyn

ENTRYPOINT ["/usr/sbin/inadyn", "--foreground"]
